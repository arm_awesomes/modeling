# Unified Modeling Language统一建模语言

## What is UML

`UML(Unified Modeling Language, 统一建模语言)`是一种由一整套图表组成的标准化建模语言，

## Diagrams in UML

`UML2.5`中共定义了14种图形，这14种图形可以分别归类到`structure diagram结构图`和`behavior diagram行为图`：

+ 结构图用于描述系统的结构
+ 行为图用于描述系统的行为

![the_taxonomy_of_structure_and_behavior_diagrams](./resources/image/the_taxonomy_of_structure_and_behavior_diagrams.png)

>`UML 2.5 Specification` Page 683 Figure A.5

1. 最常用的是用例图
1. 其次是活动图、状态图、顺序图和类图
1. 再其次是对象图、协作图、组件图和部署图

### Structure Diagrams

1. :star2: Class Diagram - Structured Classifiers类图：描述 **类** 之间的关系
1. Object Diagram - Classification对象图：描述 **某个具体时间** 时 **对象**之间的关系
1. Package Diagram - Packages包图：描述 **包** 的结构以及 **包** 之间的依赖关系
1. Component Diagram - Structured Classifiers组件图/构件图：描述 **构件** 的结构以及 **构件** 之间的依赖关系
1. Composite Structure Diagram - Structured Classifiers组合结构图：描述系统的硬件配置和部署以及软件的构件和模块在不同节点上分布的模型图。它能够帮助系统相关人员了解系统中各个构件部署在什么硬件上以及硬件之间的交互关系
1. Deployment diagram - Deployments部署图：描述系统中物理结构
1. Profile Diagram - Packages制品图：描述系统的物理结构，制品包括文件、数据库等，制品图通常与部署图一起使用。

### Behavior Diagrams

1. :star2: Use Case Diagram - Use Cases用例图：描述用户是如何使用一个系统的，是用户所能观察和使用到的系统功能的模型图
1. :star2: Activity Diagram - Activities活动图：描述满足用例要求所要进行的活动以及活动间的约束关系，有利于识别并行活动
1. :star2: State Machine Diagram - State Machines状态图/状态机图：描述一个对象所处的可能状态以及状态之间的转移。用状态图建模可以帮助开发人员分析复杂对象的各种状态的转换，以及对象何时执行怎样的动作。
1. :star2: Sequence Diagram - Interactions顺序图/时序图：描述对象之间传送消息的时间顺序，是用来表示用例中的行为顺序
1. Communication Diagram - Interactions通信图：描述对象之间如何进行交互以执行特点用例或用例中特点部分行为的交互图，它强调的是发送和接收消息的对象之间的组织结构
1. Interaction Overview Diagram - Interactions：顺序图 + 活动图
1. Timing Diagram - Interactions：描述消息跨越不同对象或参与者的实际时间，而不仅仅只关心消息的相对顺序

<!--
### Other Classification

1. 用例图：指的是从用户角度来描述系统功能，并且指明各功能操作者
1. 静态图：包含的是类图和对象图。类图是一种静态模型类型，是用来表示类之间的联系、类的属性以及操作，在系统的整个生命周期都是有效的。对象图则是类图的一个实例，使用的标识几乎与类图一致，但是其生命周期有限，只能在系统中某一时间段内存在
1. 交互图：包含的是时序图和协作图，是用来描述对象之间的交互关系。时序图强调的是对象之间的消息发送顺序，是对象之间动态合作关系。协作图则是用来描述对象之间的协作关系，既显示对象间的动态合作关系，又显示对象以及它们之间的关系。时序图用来强调时间和顺序，协作图则用来强强调上下级的关系
1. 行为图：包含的是活动图和状态图，是用来描述系统的动态模型于组成对象之间的交互关系。活动图描述的是为了满足用例要求所进行的活动以及活动间的约束关系，方便识别并进行活动。状态图是类的补充，是用来描述类的对象所有可能的状态以及事件发生时状态的转移条件
1. 实现图：包含的是部署图和组件图。部署图是用来表示建模系统的物理部署。组件图则用来表示建模软件的组织以及其相互之间的关系
-->

## Bibliographies

1. 什么是统一建模语言（UML）？. <https://www.visual-paradigm.com/cn/guide/uml-unified-modeling-language/what-is-uml/>
1. Unified Modeling Language. <https://en.wikipedia.org/wiki/Unified_Modeling_Language>
1. The Unified Modeling Language. <https://www.uml-diagrams.org/>
1. 什么是UML图？常见的UML图有哪些？. <https://www.edrawsoft.cn/uml-diagram-introduction/>
1. UML2.5 Specification.
    1. <https://www.omg.org/spec/UML/>
    1. <https://www.omg.org/spec/UML/2.5/PDF>. 2015/03/01
1. Wolf - UML建模. <https://www.cnblogs.com/wolf-sun/category/531196.html>
1. UML14种图概要介绍. <https://www.jianshu.com/p/f5ec8f529a2b>
