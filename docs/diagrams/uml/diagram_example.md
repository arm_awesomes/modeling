# UML Diagram Examples

## Use Case Diagram

![](./resources/diagram/use-case-example-online-shopping.png)

><https://www.uml-diagrams.org/examples/online-shopping-use-case-diagram-example.html>

## Activity Diagram

![](./resources/diagram/activity-example-ticket-vending-machine.png)

><https://www.uml-diagrams.org/ticket-vending-machine-activity-diagram-example.html?context=activity-examples>

## State Machine Diagram

![](./resources/diagram/online-shopping-user-account-state-diagram-example.png)

><https://www.uml-diagrams.org/examples/online-shopping-user-account-state-diagram-example.html?context=stm-examples>

## Sequence Diagram

![](./resources/diagram/sequence-examples-online-bookshop.png)

><https://www.uml-diagrams.org/online-shopping-uml-sequence-diagram-example.html?context=seq-examples>

## Class Diagram

![](./resources/diagram/class-example-online-shopping-domain.png)

><https://www.uml-diagrams.org/examples/online-shopping-domain-uml-diagram-example.html?context=cls-examples>
